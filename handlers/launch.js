module.exports = async (event) => {
  return {
    statusCode: 200,
    body: JSON.stringify({
      message: 'Successfully got to the launch handler',
      input: event,
    }, null, 2),
  };
};
